<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web' ,'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    
    Route::auth();

    Route::get('/login', ['as' => 'admin.login', 'uses' => 'LoginController@index']);
    Route::post('/login', ['as' => 'admin.authenticate', 'uses' => 'LoginController@authenticate']);
    Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'LoginController@logout']);

    Route::group(['middleware' => 'auth'], function () {

        // Dashboard Admin
        Route::get('/dashboard', ['as' => 'admin.index', 'uses' => 'DashboardController@index']);

        // User Admin Account
        Route::get('/user/profile/{id}', ['as' => 'admin.user.profile', 'uses' => 'UserAdminController@userProfile']);
        Route::get('/user/profile/edit/{id}', ['as' => 'admin.user.profile.edit', 'uses' => 'UserAdminController@userProfileEdit']);
        Route::post('/user/profile//edit{id}', ['as' => 'admin.user.profile.update', 'uses' => 'UserAdminController@userProfileUpdate']);

        // Users
        Route::get('/users/list', ['as' => 'admin.users.list', 'uses' => 'UserAdminController@usersList']);
        Route::get('/users/create', ['as' => 'admin.users.create', 'uses' => 'UserAdminController@create']);
        Route::post('/users/create', ['as' => 'admin.users.save', 'uses' => 'UserAdminController@save']);
        Route::get('/users/delete/{id}', ['as' => 'admin.users.delete', 'uses' => 'UserAdminController@delete']);
    });

});