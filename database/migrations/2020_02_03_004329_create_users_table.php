<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('system_profile_id')->default(2);
            $table->string('name', 120);
            $table->string('email', 120)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('username', 60)->unique();
            $table->dateTime('last_access')->nullable();
            $table->string('image_user', 100)->unique()->nullable();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('system_profile_id')
                ->references('id')
                ->on('system_profiles')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
