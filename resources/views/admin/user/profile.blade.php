@extends('admin.template.template')

@section('title', 'Perfil')

@section('breadcrumb')

<!-- Breadcrumbs-->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Dashboard</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Perfil</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
<!-- End Breadcrumbs-->

@endsection

<!-- Start Content-->
@section('content')

<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="mt-4"> <img src="{{ $user->image_user ? asset('/storage/users/'.$user->image_user)  : '/imagens/user-perfil-padrao.jpeg' }}" class="rounded-circle" width="150">
                    <h4 class="card-title mt-2">{{ $user->name }}</h4>
                    <h6 class="card-subtitle">Este usuário é {{ $user->system_name }} do site</h6>
                    <a class="btn btn-info {{ $user->id !== Auth::user()->id ? 'hide' : '' }}" href="{{ route('admin.user.profile.edit', Auth::user()->id) }}">Editar Perfil</a>
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body"> 
                <small class="text-muted">E-mail </small>
                <h6>{{ $user->email }}</h6> 
                <small class="text-muted pt-4 db">Telefone</small>
                <h6>{{ $user->phone ? $user->phone : 'Não Informado' }}</h6> 
                <small class="text-muted pt-4 db">Endereço</small>
                <h6>{{ $user->address ? $user->address : 'Não Informado' }}</h6>
                <small class="text-muted pt-4 db">Cidade</small>
                <h6>{{ $user->city ? $user->city : 'Não Informado' }}</h6>
                <br>
                <button class="btn btn-circle btn-secondary"><i class="fab fa-facebook-f"></i></button>
                <button class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></button>
                <button class="btn btn-circle btn-secondary"><i class="fab fa-youtube"></i></button>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Tabs -->
            <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="true">Perfil</a>
                </li>
            </ul>
            <!-- Tabs -->
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nome Completo</strong>
                                <br>
                                <p class="text-muted">{{ $user->name }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Telefone</strong>
                                <br>
                                <p class="text-muted">{{ $user->phone ? $user->name : 'Não Informado' }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                <br>
                            <p class="text-muted">{{ $user->email }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6"> <strong>Cidade</strong>
                                <br>
                                <p class="text-muted">{{ $user->city ? $user->city : 'Não Informado' }}</p>
                            </div>
                        </div>
                        <hr>
                        <div>
                            <strong>Descrição</strong>
                            <br>
                            <p class="mt-4">{{ $user->description ? $user->description : 'Sem descrição...' }}</p>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title text-uppercase">Membro desde:</h5>
                                        <div class="d-flex align-items-center mb-2 mt-4">
                                            <h2 class="mb-0 display-5"><i class=" icon-calender text-info"></i></h2>
                                            <div class="ml-auto">
                                                <h2 class="mb-0 display-7"><span class="font-normal">{{ \App\Helpers\Date::conversionOnlyDate($user->created_at) }}</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title text-uppercase">Conexões:</h5>
                                        <div class="d-flex align-items-center mb-2 mt-4">
                                            <h2 class="mb-0 display-5"><i class="fab fa-connectdevelop text-primary"></i></h2>
                                            <div class="ml-auto">
                                                <h2 class="mb-0 display-7"><span class="font-normal">{{ $user->conect ? $user->conect.' 0 Conexões' : '0 Conexões' }}</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title text-uppercase">Postagens:</h5>
                                        <div class="d-flex align-items-center mb-2 mt-4">
                                            <h2 class="mb-0 display-5"><i class="far fa-newspaper text-danger"></i></h2>
                                            <div class="ml-auto">
                                                <h2 class="mb-0 display-7"><span class="font-normal">311 Postagens</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title text-uppercase">Compras</h5>
                                        <div class="d-flex align-items-center mb-2 mt-4">
                                            <h2 class="mb-0 display-5"><i class="fas fa-shopping-cart text-success"></i></h2>
                                            <div class="ml-auto">
                                                <h2 class="mb-0 display-7"><span class="font-normal">117 Compras</span></h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>

@endsection
<!-- End Content-->