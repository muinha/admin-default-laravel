@extends('admin.template.template')

@section('title', 'Dashboard')

@section('breadcrumb')
    
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-light">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Lista de Usuarios</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0 bg-light">
                        <li class="breadcrumb-item"><a href="{{ route('admin.index') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Lista de Usuários</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->

@endsection

@section('content')
@include('messages')
<div class="card">
    <div class="col-md-12">
        @include('messages')
    </div>
    <div class="card-body">
        <div style="margin: 15px 0px 15px 0px;">
            <a href="{{ route('admin.users.create') }}" class="btn btn-success btn-lg text-white">Novo Usuário<i style="margin-left: 10px;" class="fa fa-plus"></i> </a>
        </div>
        <div class="table-responsive">
            <table id="demo-foo-row-toggler" class="table table-bordered footable footable-1 breakpoint breakpoint-lg" data-toggle-column="first" style="">
                <thead>
                    <tr class="footable-header">           
                        <th>ID</th>
                        <th>Nome</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Ultimo Acesso</th>
                        <th class="text-center">Perfil Do Sístema</th>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach ($users as $user)
                        <tr>                                
                        <td>{{ $user->id }}</td>
                            <td >
                                <a href="{{ route('admin.user.profile', $user->id) }}">
                                    <img src="{{ $user->image_user ? asset('/storage/users/'.$user->image_user)  : '/imagens/user-perfil-padrao.jpeg' }}" alt="user" width="40" class="rounded-circle"> {{ $user->name }}
                                </a>
                            </td>
                            <td  class="text-center">{{ $user->email }}</td>
                            <td  class="text-center">{{ $user->last_access }}</td>
                            <td  class="text-center">{{ $user->system_name }}</td>
                            <td  class="text-center">
                                <a href="{{ route('admin.user.profile', $user->id) }}" class="btn btn-primary btn-circle"><i class="fa fa-user"></i> </a>
                                <a href="{{ route('admin.user.profile.edit', $user->id) }}" class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                <a href="{{ route('admin.users.delete', $user->id) }}" class="btn btn-danger btn-circle btn-delete"><i class="fa fa-trash"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div style="margin: 5px;">
            {{ $users->links() }}
        </div>
    </div>
</div>

@endsection